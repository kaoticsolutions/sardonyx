load 'src/ircsocket.rb'
load 'src/hook.rb'
require 'json'

class Bot

	def initialize(address, port, nick, ident, real_name)
		$address = address
		$port = port

		$nick = nick
		$ident = ident
		$realname = real_name

		$parsers = []

		initHook()
		initConfig()
		initParsers()
		initModule()
		initSocket()

		bot = self

		listen()
	end

	def initHook()
		$hooks = Hook.new()
		$hooks.add("AFTER_HOOK")

		$hooks.add("BEFORE_CONFIG")
		$hooks.add("AFTER_CONFIG")


		$hooks.add("BEFORE_PARSERS")
		$hooks.add("AFTER_PARSERS")

		$hooks.add("BEFORE_MODULE")
		$hooks.add("AFTER_MODULE")

		$hooks.add("BEFORE_CONNECT")
		$hooks.add("AFTER_CONNECT")

		$hooks.register("AFTER_PARSERS", method(:addCoreHooks))
	end

	def initConfig()
		$hooks.trigger("BEFORE_CONFIG")

		$hooks.trigger("AFTER_CONFIG")
	end

	def initParsers()
		$hooks.trigger("BEFORE_PARSERS")
		file = File.read("parsers/irc.json")
		@ircParser = JSON.parse(file)
		$hooks.trigger("AFTER_PARSERS")
	end

	def addCoreHooks()
		@ircParser.each { |parse|
			name = parse['name']

			$hooks.add(name)
		}

		$hooks.register("hostnameLookup", method(:ircRegister))
		$hooks.register("endOfMOTD", method(:ircCoreCommands))
		$hooks.register("ping", method(:pong))
		$hooks.register("privmsg", method(:testprivmsg))
	end

	def testprivmsg(nick, ident, host, destination, message)
		msg(destination, "#{nick}!#{ident}@#{host} IN #{destination} SAID #{message}")
	end

	def ircCoreCommands()
		join("#bots")
	end

	def ircRegister()
		nick($nick)
		user($ident, $realname)
	end

	def initModule()

	end

	def initSocket()
		$hooks.trigger("BEFORE_CONNECT")
		$ircsocket = IrcSocket.new($address, $port)
		$ircsocket.connect($address, $port)
		$hooks.trigger("AFTER_CONNECT")
	end

	def listen()
		while(line = $socket.gets())
			puts(line)

			@ircParser.each { |parse|
				name = parse['name']
				regex = Regexp.new(parse['regex'])

				match = /#{regex}/.match(line)
				if (match)
					if (name == "ping")
						puts match.captures
					end
					if (match.captures.size > 1)
						$hooks.trigger(name, match.captures)
					else
						$hooks.trigger(name)
					end
				end
			}

		end
	end

	def nick(nick)
		$nick = nick

		raw("NICK #{$nick}")
	end

	def user(ident, realname)
		$ident = ident
		$realname = realname

		raw("USER #{$ident} * 8 :#{$realname}")
	end

	def pong(server)
		raw("PONG :#{server}")
	end

	def msg(destination, message)
		raw("PRIVMSG #{destination} :#{message}")
	end

	def join(channel, password = nil)
		raw("JOIN #{channel}")
	end

	def raw(message)
		$socket.write("#{message}\n")
	end

end