class Hook

	@@hooks = []
	@hookMap = []

	def initialize
		@@hooks = []
		@hookMap = []
	end

	def add(hook)
		@hookMap.push(hook)
		key = @hookMap.index(hook)
		@@hooks[key] = []
	end

	def register(hook, callback, search = nil)
		key = @hookMap.index(hook)

		@@hooks[key].push([hook, callback, search])
	end

	def trigger(hook, args = nil)
		key = @hookMap.index(hook)

		hooks = @@hooks[key]

		hooks.each { |hook|
			Thread.new {
				if (args)
					hook[1].call(*args)
				else
					hook[1].call()
				end

			}
		}
	end

end