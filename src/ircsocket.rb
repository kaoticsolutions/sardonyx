class IrcSocket

	def initialize(address, port, ssl = false)
		$address = address
		$port = port
		$ssl = ssl
	end

	def connect(address, port, ssl = false)
		$address = address
		$port = port
		$ssl = ssl

		$socket = TCPSocket.new($address, $port)
	end

	def disconnect()
		$socket.close()
	end

	def read()
		line = $socket.gets
		if (line)
			return line
		end

	end

	def write(line)
		puts("WROTE #{line}")
		$socket.puts(line)
	end

end